﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RestSharp;
using Newtonsoft.Json.Linq;
using MWork.Tracker.Domain.Models;
using MWork.Tracker.Domain.Interfaces;

namespace MWork.Tracker.Provider.PocztaPolska.Services
{
    public class PocztaPolskaService : ITrackProvider
    {
        private RestClient restClient;
        
        public PocztaPolskaService()
        {
            this.restClient = new RestClient("http://192.168.1.22/PPTrackingSoapClient");
        }

        public TrackingStatus Track(Parcel parcel)
        {
            TrackingStatus trackingStatus = new TrackingStatus();
            
            var request = new RestRequest("Api/Track/{number}", Method.GET);
            request.AddUrlSegment("number", parcel.TrackingNumber);
            IRestResponse response = restClient.Execute(request);

            trackingStatus.JsonStatus = response.Content;
            trackingStatus.Status = this.GetStatusFromJson(trackingStatus.JsonStatus);

            return trackingStatus;
        }

        private TrackingStatus.TRACKING_STATUS GetStatusFromJson(string json)
        {
            try
            {
                JObject ob = JObject.Parse(json);

                TrackingStatus.TRACKING_STATUS status;

                switch (int.Parse(ob["statusField"].ToString()))
                {
                    case 0:
                        status = (bool)ob["danePrzesylkiField"]["zakonczonoObslugeField"] ? TrackingStatus.TRACKING_STATUS.FINISHED : TrackingStatus.TRACKING_STATUS.OK;
                        break;
                    case -1:
                        status = TrackingStatus.TRACKING_STATUS.NOT_FOUND;
                        break;
                    default:
                        status = TrackingStatus.TRACKING_STATUS.ERROR;
                        break;
                }

                return status;
            }
            catch
            {
                throw;
            }
        }
        
    }
}
