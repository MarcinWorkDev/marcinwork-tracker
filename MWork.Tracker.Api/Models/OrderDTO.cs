﻿using System.Collections.Generic;

namespace MWork.Tracker.Api.Models
{
    public class OrderDTO
    {
        public int Id { get; set; }

        public string UserId { get; set; }

        public string Name { get; set; }

        public string Note { get; set; }

        public string Url { get; set; }
        
        public virtual IEnumerable<ParcelDTO> Parcels { get; set; }
    }
}
