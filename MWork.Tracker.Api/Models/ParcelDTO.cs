﻿using System;

namespace MWork.Tracker.Api.Models
{
    public class ParcelDTO
    {
        public int Id { get; set; }

        public string TrackingNumber { get; set; }

        public int ProviderCompany_Id { get; set; }

        public ProviderCompanyDTO ProviderCompany { get; set; }

        public int Order_Id { get; set; }

        public OrderDTO Order { get; set; }

        public DateTime LastCheckTimestamp { get; set; }

        public string LastCheckResult { get; set; }

        public Boolean Finished { get; set; }
    }
}
