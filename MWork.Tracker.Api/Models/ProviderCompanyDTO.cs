﻿namespace MWork.Tracker.Api.Models
{
    public class ProviderCompanyDTO
    {
        public int Id { get; set; }
        
        public string Name { get; set; }

        public string PluginServiceName { get; set; } 
    }
}
