﻿namespace MWork.Tracker.Api.Models
{
    public class TrackingStatusDTO
    {
        public enum TRACKING_STATUS
        {
            OK, // monitorowane
            FINISHED, // dostarczone
            NOT_FOUND, // nieznalezione
            ERROR // blad
        }

        public ParcelDTO Tracking { get; set; }

        public string JsonStatus { get; set; }

        public TRACKING_STATUS Status { get; set; }
    }
}
