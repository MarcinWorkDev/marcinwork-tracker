﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Authorization;
using MWork.Tracker.Domain.Interfaces;
using MWork.Tracker.Domain.Models;

namespace MWork.Tracker.Api.Controllers
{
    [Authorize(Policy = "level:admin")]
    [Produces("application/json")]
    [Route("api/v1/[controller]/[action]")]
    public class ProviderCompanyController : Controller
    {
        private readonly IProviderCompanyRepository _providerCompanyRepository; // TODO: przerobić na DTO!

        public ProviderCompanyController(IProviderCompanyRepository providerCompanyRepository)
        {
            _providerCompanyRepository = providerCompanyRepository;
        }

        /// <summary>
        /// Get all
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetAll()
        {
            return Ok(_providerCompanyRepository.GetAll());
        }
        
        /// <summary>
        /// Get one
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            return Ok(_providerCompanyRepository.GetOne(id));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="providerCompany"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Add(ProviderCompany providerCompany)
        {
            _providerCompanyRepository.Add(providerCompany);
            return CreatedAtAction("Get", new { providerCompany.Id }, providerCompany);
        }

        [HttpPatch("{id}")]
        public IActionResult Update(int id, [FromBody]JsonPatchDocument<ProviderCompany> providerCompanyPatch)
        {
            ProviderCompany providerCompany = _providerCompanyRepository.GetOne(id);
            if (providerCompany == null)
            {
                NotFound();
            }

            providerCompanyPatch.ApplyTo(providerCompany);

            _providerCompanyRepository.Update(providerCompany);
            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _providerCompanyRepository.Delete(id);
            return Ok();
        }

    }
}