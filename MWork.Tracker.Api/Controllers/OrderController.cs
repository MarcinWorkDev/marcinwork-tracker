﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Authorization;
using MWork.Tracker.Domain.Interfaces;
using MWork.Tracker.Domain.Models;

namespace MWork.Tracker.Api.Controllers
{
    [Authorize(Policy = "level:user")]
    [Produces("application/json")]
    [Route("api/v1/[controller]/[action]")]
    public class OrderController : Controller
    {
        private readonly IOrderRepository _orderRepository; // TODO: przerobić na DTO!

        public OrderController(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        /// <summary>
        /// Get all shipments
        /// </summary>
        /// <returns></returns>
        [Authorize(Policy = "level:admin")]
        [HttpGet]
        public IActionResult GetAll()
        {
            IQueryable<Order> order = _orderRepository.GetAll();
            if (order.Count() > 0)
            {
                return Ok(order);
            } else
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Get one shipment
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            Order order = _orderRepository.GetOne(id);
            if (order.Id > 0)
            {
                return Ok(order);
            }
            else
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Add new shipment
        /// </summary>
        /// <param name="shipment"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Add([FromBody]Order order)
        {
            _orderRepository.Add(order);
            return CreatedAtAction("Get", new { order.Id }, order);
        }

        /// <summary>
        /// Update existing shipment
        /// </summary>
        /// <param name="id"></param>
        /// <param name="shipmentPatch"></param>
        /// <returns></returns>
        [HttpPatch("{id}")]
        public IActionResult Update(int id, [FromBody]JsonPatchDocument<Order> orderPatch)
        {
            Order shipment = _orderRepository.GetOne(id);
            if (shipment == null)
            {
                NotFound();
            }

            orderPatch.ApplyTo(shipment);

            _orderRepository.Update(shipment);
            return Ok();
        }

        /// <summary>
        /// Delete existing shipment
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _orderRepository.Delete(id);
            return Ok();
        }
    }
}