﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Authorization;
using MWork.Tracker.Domain.Interfaces;
using MWork.Tracker.Domain.Models;

namespace MWork.Tracker.Api.Controllers
{
    [Authorize(Policy = "level:user")]
    [Produces("application/json")]
    [Route("api/v1/[controller]/[action]")]
    public class ParcelController : Controller
    {
        private readonly IParcelRepository _parcelRepository; // TODO: przerobić na DTO!

        public ParcelController(IParcelRepository parcelRepository)
        {
            _parcelRepository = parcelRepository;
        }

        /// <summary>
        /// Get all trackings
        /// </summary>
        /// <returns></returns>
        [Authorize(Policy = "level:admin")]
        [HttpGet]
        public IActionResult GetAll()
        {
            return Ok(_parcelRepository.GetAll());
        }
        
        /// <summary>
        /// Get one tracking
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public IActionResult Get(string id)
        {
            return Ok(_parcelRepository.GetOne(id));
        }

        /// <summary>
        /// Add new tracking
        /// </summary>
        /// <param name="tracking"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Add([FromBody]Parcel parcel)
        {
            _parcelRepository.Add(parcel);
            return CreatedAtAction("Get", new { parcel.TrackingNumber }, parcel);
        }

        /// <summary>
        /// Update existing tracking
        /// </summary>
        /// <param name="id"></param>
        /// <param name="trackingPatch"></param>
        /// <returns></returns>
        [HttpPatch("{id}")]
        public IActionResult Update(string id, [FromBody]JsonPatchDocument<Parcel> parcelPatch)
        {
            Parcel parcel = _parcelRepository.GetOne(id);
            if (parcel == null)
            {
                NotFound();
            }

            parcelPatch.ApplyTo(parcel);

            _parcelRepository.Update(parcel);
            return Ok();
        }

        /// <summary>
        /// Delete existing tracking
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _parcelRepository.Delete(id);
            return Ok();
        }
    }
}