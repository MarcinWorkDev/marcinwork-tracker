﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication;
using RestSharp;

namespace MarcinWorkTracker.Presentation.Controllers
{
    public class TrackingController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult GetAll()
        {
            string accToken = HttpContext.GetTokenAsync("access_token").Result;
            string idToken = HttpContext.GetTokenAsync("id_token").Result;

            var client = new RestClient("https://core.marcin.work/TrackerApi/api/v1/");
            var request = new RestRequest("Tracking/GetAll", Method.GET);
            request.AddHeader("Authentication", $"Bearer {accToken}");

            var response = client.Execute(request);

            return View("GetAll", response);
        }
    }
}