﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MWork.Tracker.Data.Models
{
    public class OrderDTO
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public string UserId { get; set; }

        //[ForeignKey("UserId")]
        //public ApplicationUser User { get; set; }

        [Required]
        public string Name { get; set; }

        public string Note { get; set; }

        public string Url { get; set; }
        
        public virtual IEnumerable<ParcelDTO> Parcels { get; set; }
    }
}
