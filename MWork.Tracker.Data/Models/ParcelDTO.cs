﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MWork.Tracker.Data.Models
{
    public class ParcelDTO
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string TrackingNumber { get; set; }

        public int ProviderCompany_Id { get; set; }

        //[ForeignKey("ProviderCompany_Id")]
        public ProviderCompanyDTO ProviderCompany { get; set; }

        public int Parcel_Id { get; set; }

        public OrderDTO Parcel { get; set; }

        public DateTime LastCheckTimestamp { get; set; }

        public string LastCheckResult { get; set; }

        public Boolean Finished { get; set; }
    }
}
