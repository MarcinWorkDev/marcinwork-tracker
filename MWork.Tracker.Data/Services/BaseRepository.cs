﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using MWork.Tracker.Data;
using MWork.Tracker.Data.Models;
using MWork.Tracker.Domain.Interfaces;
using MWork.Tracker.Domain.Models;

namespace MWork.Tracker.Data.Services
{
    public class BaseRepository
    {
        protected readonly MWTrackerDbContext _context;
        protected IMapper mapper;

        public BaseRepository(MWTrackerDbContext context)
        {
            _context = context;

            var mapperConfiguration = new MapperConfiguration(config =>
            {
                config.CreateMap<ParcelDTO, Parcel>().ReverseMap();
                config.CreateMap<OrderDTO, Order>().ReverseMap();
                config.CreateMap<ProviderCompanyDTO, ProviderCompany>().ReverseMap();
            });

            mapperConfiguration.AssertConfigurationIsValid();
            mapper = mapperConfiguration.CreateMapper();
        }
    }
}
