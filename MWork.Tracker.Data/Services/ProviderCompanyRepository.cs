﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MWork.Tracker.Data.Models;
using MWork.Tracker.Data;
using MWork.Tracker.Domain.Interfaces;
using MWork.Tracker.Domain.Models;

namespace MWork.Tracker.Data.Services
{
    public class ProviderCompanyRepository : BaseRepository, IProviderCompanyRepository
    {
        public ProviderCompanyRepository(MWTrackerDbContext context) : base(context)
        {

        }

        public IQueryable<ProviderCompany> GetAll()
        {
            return mapper.Map<IQueryable<ProviderCompany>>(_context.ProviderCompanies);
        }

        public ProviderCompany GetOne(int providerCompanyId)
        {
            return mapper.Map<ProviderCompany>(_context.ProviderCompanies.FirstOrDefault(e => e.Id == providerCompanyId));
        }

        public void Add(ProviderCompany providerCompany)
        {
            _context.ProviderCompanies.Add(mapper.Map<ProviderCompanyDTO>(providerCompany));
            _context.SaveChanges();
        }

        public void Update(ProviderCompany providerCompany)
        {
            _context.ProviderCompanies.Update(mapper.Map<ProviderCompanyDTO>(providerCompany));
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            _context.ProviderCompanies.Remove(_context.ProviderCompanies.Where(x => x.Id == id).FirstOrDefault());
        }
    }
}
