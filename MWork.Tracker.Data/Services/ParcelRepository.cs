﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MWork.Tracker.Data;
using MWork.Tracker.Data.Models;
using MWork.Tracker.Domain.Interfaces;
using MWork.Tracker.Domain.Models;
using AutoMapper;

namespace MWork.Tracker.Data.Services
{
    public class ParcelRepository : BaseRepository, IParcelRepository
    {
        public ParcelRepository(MWTrackerDbContext context) : base(context)
        {

        }

        public void Add(Parcel tracking)
        {
            _context.Parcels.Add(mapper.Map<ParcelDTO>(tracking));
            _context.SaveChanges();
        }
        public void Update(Parcel tracking)
        {
            _context.Parcels.Update(mapper.Map<ParcelDTO>(tracking));
            _context.SaveChanges();
        }

        public IQueryable<Parcel> GetAll()
        {
            return mapper.Map<IQueryable<Parcel>>(_context.Parcels.Include(e => e.ProviderCompany).OrderByDescending(e => e.LastCheckTimestamp));
        }
        
        public Parcel GetOne(String trackingNumber)
        {
            return mapper.Map<Parcel>(_context.Parcels.Include(e => e.ProviderCompany).FirstOrDefault(e => e.TrackingNumber == trackingNumber));
        }

        public Parcel GetOne(int parcelId)
        {
            return mapper.Map<Parcel>(_context.Parcels.Include(e => e.ProviderCompany).FirstOrDefault(e => e.Id == parcelId));
        }

        public void Delete(int parcelId)
        {
            _context.Parcels.Remove(_context.Parcels.Where(x => x.Id == parcelId).FirstOrDefault());
        }
    }
}
