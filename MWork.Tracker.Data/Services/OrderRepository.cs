﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MWork.Tracker.Data;
using MWork.Tracker.Domain.Interfaces;
using MWork.Tracker.Domain.Models;
using MWork.Tracker.Data.Models;

namespace MWork.Tracker.Data.Services
{
    public class OrderRepository : BaseRepository, IOrderRepository
    {
        public OrderRepository(MWTrackerDbContext context) : base(context)
        {

        }

        public IQueryable<Order> GetAll()
        {
            return mapper.Map<IQueryable<Order>>(_context.Orders.OrderByDescending(e => e.Id));
        }

        public Order GetOne(int orderId)
        {
            return mapper.Map<Order>(_context.Orders.Include(e => e.Parcels).ThenInclude(e => e.ProviderCompany).FirstOrDefault(e => e.Id == orderId));
        }

        public void Add(Order order)
        {
            _context.Orders.Add(mapper.Map<OrderDTO>(order));
            _context.SaveChanges();
        }
        public void Update(Order order)
        {
            _context.Orders.Update(mapper.Map<OrderDTO>(order));
            _context.SaveChanges();
        }
        public void Delete(int id)
        {
            _context.Orders.Remove(_context.Orders.Where(x => x.Id == id).FirstOrDefault());
        }
    }
}
