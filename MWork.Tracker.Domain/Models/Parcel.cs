﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWork.Tracker.Domain.Models
{
    public class Parcel
    {
        public int Id { get; set; }
        public string TrackingNumber { get; set; }
        public int ProviderCompany_Id { get; set; }
        public ProviderCompany ProviderCompany { get; set; }
        public int Order_Id { get; set; }
        public Order Order { get; set; }
        public DateTime? LastCheckTimestamp { get; set; }
        public string LastCheckResult { get; set; }
        public Boolean Finished { get; set; }
    }
}
