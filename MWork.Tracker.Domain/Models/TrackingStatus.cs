﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWork.Tracker.Domain.Models
{
    public class TrackingStatus
    {
        public enum TRACKING_STATUS
        {
            OK, // monitorowane
            FINISHED, // dostarczone
            NOT_FOUND, // nieznalezione
            ERROR // blad
        }

        public Parcel Tracking { get; set; }
        public string JsonStatus { get; set; }
        public TRACKING_STATUS Status { get; set; }
    }
}
