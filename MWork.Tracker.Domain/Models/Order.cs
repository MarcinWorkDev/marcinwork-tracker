﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWork.Tracker.Domain.Models
{
    public class Order
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        //[ForeignKey("UserId")]
        //public ApplicationUser User { get; set; }
        public string Name { get; set; }
        public string Note { get; set; }
        public string Url { get; set; }
        
        public virtual IEnumerable<Parcel> Parcels { get; set; }
    }
}
