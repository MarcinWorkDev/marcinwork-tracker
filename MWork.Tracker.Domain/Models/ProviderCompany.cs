﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWork.Tracker.Domain.Models
{
    public class ProviderCompany
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string PluginServiceName { get; set; } 
    }
}
