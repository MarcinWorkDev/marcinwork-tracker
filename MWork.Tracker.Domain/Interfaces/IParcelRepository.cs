﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MWork.Tracker.Domain.Models;

namespace MWork.Tracker.Domain.Interfaces
{
    public interface IParcelRepository
    {
        Parcel GetOne(int parcelId);
        Parcel GetOne(string trackingNumber);

        IQueryable<Parcel> GetAll();

        void Add(Parcel parcel);
        void Update(Parcel parcel);
        void Delete(int parcelId);
    }
}
