﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MWork.Tracker.Domain.Models;

namespace MWork.Tracker.Domain.Interfaces
{
    public interface IOrderRepository
    {
        Order GetOne(int orderId);

        IQueryable<Order> GetAll();

        void Add(Order order);
        void Update(Order order);
        void Delete(int id);
    }
}
