﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MWork.Tracker.Domain.Models;

namespace MWork.Tracker.Domain.Interfaces
{
    public interface IProviderCompanyRepository
    {
        ProviderCompany GetOne(int providerCompanyId);
        IQueryable<ProviderCompany> GetAll();
        void Add(ProviderCompany providerCompany);
        void Update(ProviderCompany providerCompany);
        void Delete(int id);
    }
}
