﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MWork.Tracker.Domain.Models;

namespace MWork.Tracker.Domain.Interfaces
{
    public interface ITrackProvider
    {
        TrackingStatus Track(Parcel tracking);
    }
}
