﻿using System;
using System.Collections.Generic;
using System.Text;
using MWork.Tracker.Domain.Interfaces;
using MWork.Tracker.Domain.Models;

namespace MWork.Tracker.Domain.Services
{
    public class ShimpentTrackingService
    {
        private TrackingStatus TrackByProvider(Parcel tracking)
        {
            TrackingStatus trackingStatus = new TrackingStatus();

            try
            {
                ITrackProvider trackProvider = (ITrackProvider)(Activator.CreateInstance(Type.GetType(tracking.ProviderCompany.PluginServiceName)));
                trackingStatus = trackProvider.Track(tracking);
            }
            catch (Exception ex)
            {
                trackingStatus.Status = TrackingStatus.TRACKING_STATUS.ERROR;
                trackingStatus.JsonStatus = "{IS_ERROR = true, ERROR = " + ex.Message + "}";
            }
            return trackingStatus;
        }
    }
}
